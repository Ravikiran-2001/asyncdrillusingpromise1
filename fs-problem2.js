const fs = require("fs").promises; //importing fs module using promises
const fileNamePath = "../fileName.txt"; //assigning file path
function operations(path) {
  //creating function
  return new Promise((resolve, reject) => {
    fs.readFile(path, "utf8") //reading file content
      .then((textFile) => {
        const question1 = textFile.toUpperCase(); //converting all content to upperCase
        return fs.writeFile("../upperCaseData.txt", question1, "utf8"); //added content and returning
      })
      .then(() => fs.appendFile(fileNamePath, "upperCaseData.txt\n", "utf8")) //appending file name inside fileNames file
      .then(() => fs.readFile("../upperCaseData.txt", "utf8")) //reading file content
      .then((upperCase_file) => {
        const question2 = upperCase_file.toLowerCase(); //converting to lowerCase
        const splitingResult = question2.split("\n").join(" "); //splitting to repective to line and joining again
        fs.writeFile("../seperatedByLine.txt", splitingResult, "utf8");
      })
      .then(() => fs.appendFile(fileNamePath, "seperatedByLine.txt\n", "utf8"))
      .then(() => fs.readFile("../seperatedByLine.txt", "utf8"))
      .then((seperatedByLineFile) => {
        let words = seperatedByLineFile.split(" "); //splitting with respect to space
        let sortedWords = words.sort(); //sorting all words
        const question3 = sortedWords.join(" "); //joining all words
        return fs.writeFile("../sortedWords.txt", question3, "utf8");
      })
      .then(() => {
        fs.appendFile(fileNamePath, "sortedWords.txt\n", "utf8");
      })
      .then(() => fs.readFile("../fileName.txt", "utf-8"))
      .then((fileNames) => {
        setTimeout(() => {
          //setting timeOut
          let files = fileNames
            .split("\n") //splitting to repect to line
            .filter((value) => value.trim() !== ""); //if there are any extra spaces it will be trimmed
          const unlinkFiles = files.forEach((value) => {
            //iterating every fileName
            fs.unlink(`../${value}`); //deleting files
          });
          console.log("files deleted");
        }, 2000);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
module.exports = operations; //exporting function
