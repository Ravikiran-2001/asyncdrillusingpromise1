const fs = require("fs").promises; //importing fs module using promises
const path = require("path"); //importing path module
function createAndDeleteFiles(directoryPath, numberOfFiles) {
  //creating function
  return new Promise((resolve, reject) => {
    //returning promise
    fs.mkdir(directoryPath) //creating a directory
      .then(() => {
        for (let fileNumber = 1; fileNumber <= numberOfFiles; fileNumber++) {
          //running loop till numberOfFiles
          let fileName = `file${fileNumber}.json`; //creating file name
          let data = {
            //assigning data
            number: Math.random(),
          };
          let filePath = path.join(directoryPath, fileName); //creating file path
          fs.writeFile(filePath, JSON.stringify(data)); //storing data inside file
        }
      })
      .then(() => {
        console.log("files created");
        return fs.readdir(directoryPath); //returning content inside directory
      })
      .then((file) => {
        setTimeout(() => {
          //setting time out
          file.forEach((value) => {
            //iterating inside file
            const filePath = path.join(directoryPath, value); //assigning file path
            fs.unlink(filePath); //deleting file
          });
          console.log("deleted files");
        }, 5000);
      })
      .catch((error) => {
        console.log(error);
      });
  });
}
module.exports = createAndDeleteFiles; //exporting function
